Feature: As a user I should be able to login in my BananaTag account

  Scenario: Trying to login with a valid user
    Given that the user navigate to BananaTag website
    When the user "btag55557@gmail.com" login with a valid account
    Then the home page should be displayed
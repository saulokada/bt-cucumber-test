@now
Feature: As a user I should be able to see the sent emails and view their details

    Background:
        Given that the user navigate to BananaTag website
        When the user "btag55557@gmail.com" login with a valid account
        Then the home page should be displayed

    Scenario: Displaying user emails
        Given that the user navigate to Emails page
        And the list of all emails sent by the user are properly displayed
        When user select one of the emails
        And it should display the details from that email